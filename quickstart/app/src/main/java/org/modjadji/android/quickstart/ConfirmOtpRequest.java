package org.modjadji.android.quickstart;

import com.google.gson.annotations.Expose;

public class ConfirmOtpRequest {
    @Expose
    public String RegistrationToken;
    @Expose
    public String OTPValue;
}
