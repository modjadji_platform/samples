package org.modjadji.android.quickstart;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class ModjadjiApiClient {
    private final ModjadjiApiService mModjadjiApiService;

    private static final String MODJADJI_API_BASE_URL = "https://api.dev.modjadji.org:8243/tipsgo_dev/v1.0.0/api/";

    private final static String API_KEY = "2AMTwBLMN22sQsZGpZVlMuTFdMIa";
    private final static String API_SECRET = "MoGTl0OuvPaYw_dC1Pp8tKo4naoa";
    private final static String BASIC_TOKEN = Base64.encodeToString((API_KEY + ":" + API_SECRET).getBytes(), Base64.DEFAULT);
    /**
     *ACCESS_TOKEN value is pre-generated. However, It can be generated prby invoking following API
     * POST https://api.dev.modjadji.org:8243/token
     *
     * Headers:
     *  Authorization: Basic <BASIC_TOKEN>
     *  Content-Type: application/x-www-form-urlencoded
     *
     * Request body:
     * grant_type=client_credentials&scope=PRODUCTION
     *
     * Expected response:
     * {
     * scope: "default"
     * token_type: "bearer"
     * expires_in: 9223372036854776000
     * access_token: "82785dd168445e63c24b67b0bb713b3" <-- this is the new access token
     * }
     */

    private final static String ACCESS_TOKEN = "82785dd168445e63c24b67b0bb713b3";
    private final static String AUTHORIZATION_HEADER = "Bearer " + ACCESS_TOKEN;

    private final static String CONSUMER_WALLET_TYPE = "Consumer";
    private final static String API_SUCCESS_CODE = "000000";
    private final static String DEFAULT_PAYMENT_PIN = "123456";
    private final static String TEST_OTP = "123456";
    private final static String APP_ID = "SmartPayAndroid";

    private String mDeviceId;

    private static ModjadjiApiClient sModjadjiApiClient;

    public static ModjadjiApiClient getInstance() {
        if (sModjadjiApiClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient();
            httpClient.setConnectTimeout(150, TimeUnit.SECONDS);
            httpClient.setWriteTimeout(150, TimeUnit.SECONDS);
            httpClient.setReadTimeout(150, TimeUnit.SECONDS);
            httpClient.interceptors().add(logging);

            Gson gson = new GsonBuilder().
                    excludeFieldsWithoutExposeAnnotation()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .baseUrl(MODJADJI_API_BASE_URL)
                    .build();


            ModjadjiApiService modjadjiApiService = retrofit.create(ModjadjiApiService.class);
            sModjadjiApiClient = new ModjadjiApiClient(modjadjiApiService);
        }

        return sModjadjiApiClient;
    }

    private ModjadjiApiClient(ModjadjiApiService modjadjiApiService) {
        mModjadjiApiService = modjadjiApiService;
        mDeviceId = getDeviceId();
    }

    public String registerMember(String firstname,
                                 String middlename,
                                 String lastname,
                                 String dob,
                                 String gender,
                                 String username,
                                 String email,
                                 String gcmPushId,
                                 String phoneNumber,
                                 String countryCode,
                                 String city,
                                 String password) {
        RegisterWalletRequest request = new RegisterWalletRequest();
        request.Email = email;
        request.UserName = username;
        request.PhoneNumber = phoneNumber;
        request.FirstName = firstname;
        request.MiddleName = middlename;
        request.LastName = lastname;
        request.DateOfBirth = dob;
        request.Gender = gender;
        request.CountryCode = countryCode;
        request.City = city;
        request.PushID = gcmPushId;
        request.Password = password;

        String installationId = null;
        try {
            Response<RegisterWalletResponse> response =
                    mModjadjiApiService.registerMember(AUTHORIZATION_HEADER,
                            mDeviceId,
                            APP_ID,
                            request).execute();
            if (response.isSuccess()) {
                RegisterWalletResponse registerWalletResponse = response.body();
                if (registerWalletResponse != null &&
                        registerWalletResponse.Status.equals(API_SUCCESS_CODE) &&
                        registerWalletResponse.DataObject != null) {
                    String tokenId = registerWalletResponse.DataObject.RegistrationToken;
                    installationId = confirmOtp(tokenId, TEST_OTP); //For testing purposes, OTP is always 123456
                }
            }
        } catch (IOException ioe) {

        }

        return installationId;
    }

    private String confirmOtp(String registrationTokenId, String otp) {
        String installationId = null;
        try {
            ConfirmOtpRequest request = new ConfirmOtpRequest();
            request.OTPValue = otp;
            request.RegistrationToken = registrationTokenId;
            Response<ConfirmOtpResponse> response =
                    mModjadjiApiService.confirmOTP(AUTHORIZATION_HEADER,
                            mDeviceId,
                            APP_ID,
                            request).execute();
            if (response.isSuccess()) {
                ConfirmOtpResponse confirmOtpResponse = response.body();
                if (confirmOtpResponse != null &&
                        confirmOtpResponse.Status.equals(API_SUCCESS_CODE) &&
                        confirmOtpResponse.DataObject != null) {
                    installationId = confirmOtpResponse.DataObject.InstallationID;
                }
            }
        } catch (IOException ioe) {

        }
        return installationId;
    }


    public boolean confirmPayment(String paymentToken, String installationID) {
        ConfirmPaymentRequest request = new ConfirmPaymentRequest();
        request.PaymentToken = paymentToken;
        request.PIN = DEFAULT_PAYMENT_PIN;
        Boolean isPaymentConfirmed = false;
        try {
            Response<ConfirmPaymentResponse> response =
                    mModjadjiApiService.confirmPayment(AUTHORIZATION_HEADER,
                            mDeviceId,
                            APP_ID,
                            installationID,
                            request).execute();
            ConfirmPaymentResponse confirmPaymentResponse = response.body();

            if (confirmPaymentResponse != null &&
                    confirmPaymentResponse.Status.equals(API_SUCCESS_CODE)) {
                isPaymentConfirmed = true;
            }

        } catch (IOException ioe) {
        }

        return isPaymentConfirmed;
    }

    public String getWalletId(String installationId) {
        String walletId = "";
        try {

            //if pageNum and pageSize are both 0, all wallets are returned
            Response<GetWalletListReponse> response =
                    mModjadjiApiService.getWalletList(AUTHORIZATION_HEADER,
                    mDeviceId,  APP_ID, installationId, 0, 0).execute();

            GetWalletListReponse getWalletListReponse = response.body();

            if (getWalletListReponse != null &&
                    getWalletListReponse.Status.equals(API_SUCCESS_CODE)) {
                //TODO: temporary solution is to get the last item in the list of wallets since this is the newly created wallet. Correct way is to order the list by CreatedDate and get the latest

                if (getWalletListReponse.ListOfObjects != null && getWalletListReponse.ListOfObjects.size() > 0) {
                    walletId = getWalletListReponse.ListOfObjects.get(getWalletListReponse.ListOfObjects.size() - 1).WalletCode;
                }
            }
        } catch (IOException ioe) {

        }
        return walletId;
    }

    private static String getDeviceId() {
        TelephonyManager tm = (TelephonyManager) MainActivity.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = tm.getDeviceId();

        if (TextUtils.isEmpty(deviceId) || deviceId.equals("000000000000000")) {
            //first two case(deviceId null or length), but if it does we fallback to android id
            //last case of 000000000000000 happens when you run the app via a simulator
            deviceId = Settings.Secure.getString(MainActivity.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
        }

        return deviceId;
    }
}
