package org.modjadji.android.quickstart;

import com.google.gson.annotations.Expose;

public class RegisterWalletResponse {
    @Expose
    public String Status;
    @Expose
    public String Message;
    @Expose
    public Data DataObject;

    public static class Data {
        @Expose
        public String RegistrationToken;
    }
}
