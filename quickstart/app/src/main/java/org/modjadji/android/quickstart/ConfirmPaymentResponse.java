package org.modjadji.android.quickstart;

import com.google.gson.annotations.Expose;

public class ConfirmPaymentResponse {
    @Expose
    public String Status;
    @Expose
    public String Message;
}
