package org.modjadji.android.quickstart;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

public class GetWalletListReponse {
    @Expose
    public String Status;
    @Expose
    public String Message;
    @Expose
    public List<Wallet> ListOfObjects;

    public static class Wallet {
        @Expose
        public String WalletCode;
        @Expose
        public String ID;
    }
}
