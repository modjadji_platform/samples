package org.modjadji.android.quickstart;

import com.google.gson.annotations.Expose;

public class RegisterWalletRequest {
    @Expose
    public DeviceInfo DeviceInfo;
    @Expose
    public String Email;
    @Expose
    public String UserName;
    @Expose
    public String PhoneNumber;
    @Expose
    public String FirstName;
    @Expose
    public String MiddleName;
    @Expose
    public String LastName;
    @Expose
    public String DateOfBirth;
    @Expose
    public String Gender;
    @Expose
    public String CountryCode;
    @Expose
    public String City;
    @Expose
    public String PushID;
    @Expose
    public String Password;

    public class DeviceInfo {
        @Expose
        public String Os;
        @Expose
        public String OsVersion;
        @Expose
        public String ProductModel;
        @Expose
        public String ProductName;
        @Expose
        public Boolean IsJailbroken;
        @Expose
        public String ProductManufacturer;
    }
}


