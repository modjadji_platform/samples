package org.modjadji.android.quickstart;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

public interface ModjadjiApiService {
    @POST("member/RegisterMember")
    Call<RegisterWalletResponse> registerMember(@Header("Authorization") String authorizationToken,
                                                @Header("DeviceId") String deviceId,
                                                @Header("AppId") String appId,
                                                @Body RegisterWalletRequest request);

    @POST("member/ConfirmOTP")
    Call<ConfirmOtpResponse> confirmOTP(@Header("Authorization") String authorizationToken,
                                        @Header("DeviceId") String deviceId,
                                        @Header("AppId") String appId,
                                        @Body ConfirmOtpRequest request);

    @POST("Wallet/ConfirmPayment")
    Call<ConfirmPaymentResponse> confirmPayment(@Header("Authorization") String authorizationToken,
                                                @Header("DeviceId") String deviceId,
                                                @Header("AppId") String appId,
                                                @Header("InstallationId") String installationId,
                                                @Body ConfirmPaymentRequest request);
    @GET("wallet/GetWalletList")
    Call<GetWalletListReponse> getWalletList(@Header("Authorization") String authorizationToken,
                                             @Header("DeviceId") String deviceId,
                                             @Header("AppId") String appId,
                                             @Header("InstallationId") String installationId,
                                             @Query("pageSize") int pageSize,
                                             @Query("pageNum") int pageNum);
}
