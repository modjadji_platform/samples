package org.modjadji.android.quickstart;

import com.google.gson.annotations.Expose;

public class ConfirmPaymentRequest {
    @Expose
    public String PaymentToken;
    @Expose
    public String PIN;
}
