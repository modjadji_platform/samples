package org.modjadji.android.quickstart;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

public class ConfirmPaymentIntentService extends IntentService {
    private static final String TAG = "ConfirmPaymentIntentService";

    public ConfirmPaymentIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String paymentToken = sharedPreferences.getString(QuickstartPreferences.PAYMENT_TOKEN, "");
        String installationID = sharedPreferences.getString(QuickstartPreferences.INSTALLTION_ID, "");

        boolean confirmPaymentResult = ModjadjiApiClient.getInstance().confirmPayment(paymentToken, installationID);

        sharedPreferences.edit().putBoolean(QuickstartPreferences.PAYMENT_CONFIRMATION_RESULT, confirmPaymentResult).apply();
        Intent paymentConfirm = new Intent(QuickstartPreferences.PAYMENT_CONFIRMATION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(paymentConfirm);
    }
}
