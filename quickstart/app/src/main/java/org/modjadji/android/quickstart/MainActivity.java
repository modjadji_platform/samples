/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.modjadji.android.quickstart;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.modjadji.android.samples.com.quickstart.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private BroadcastReceiver mPaymentNotificationReceiver;
    private BroadcastReceiver mConfirmPaymentReceiver;

    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;
    private static Context sInstance;
    private Button mRegisterButton;

    public static Context getInstance() {
        return sInstance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sInstance = this;

        mRegisterButton = (Button) findViewById(R.id.register_wallet_button);
        mRegisterButton.setOnClickListener(this);


        mRegistrationProgressBar = (ProgressBar) findViewById(R.id.registrationProgressBar);
        mInformationTextView = (TextView) findViewById(R.id.informationTextView);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);

                String walletId = sharedPreferences.getString(QuickstartPreferences.WALLET_ID, "");
                if (sentToken) {
                    mInformationTextView.setText(getString(R.string.registration_complete_message, walletId));
                    if (mRegisterButton != null)
                        mRegisterButton.setVisibility(View.GONE);
                } else {
                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };

        mPaymentNotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences
                        = PreferenceManager.getDefaultSharedPreferences(context);

                String paymentMessage= sharedPreferences.getString(QuickstartPreferences.PAYMENT_MESSAGE, "Cannot receive notification");
                String paymentToken = sharedPreferences.getString(QuickstartPreferences.PAYMENT_TOKEN, "N/A");

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Modjadji Notification")
                        .setCancelable(false)
                        .setMessage(paymentMessage)
                        .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Start IntentService to register wallet
                                Intent intent = new Intent(MainActivity.this, ConfirmPaymentIntentService.class);
                                MainActivity.this.startService(intent);
                            }
                        })
                        .setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Just close the dialog
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();
            }
        };

        mConfirmPaymentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences
                        = PreferenceManager.getDefaultSharedPreferences(context);

                boolean paymentConfirmStatus = sharedPreferences.getBoolean(QuickstartPreferences.PAYMENT_CONFIRMATION_RESULT, false);

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Modjadji Notification")
                        .setCancelable(false)
                        .setMessage(paymentConfirmStatus ? "Payment has been confirmed successfully" : "Failed to confirm payment")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();
            }

        };
    }

    @Override
    public void onClick(View v) {
        if (checkPlayServices()) {
            mRegistrationProgressBar.setVisibility(View.VISIBLE);
            mInformationTextView.setText(getString(R.string.registering_message));

            // Start IntentService to register wallet
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mPaymentNotificationReceiver,
                new IntentFilter(QuickstartPreferences.PAYMENT_NOTIFICATION_RECEIVED));

        LocalBroadcastManager.getInstance(this).registerReceiver(mConfirmPaymentReceiver,
                new IntentFilter(QuickstartPreferences.PAYMENT_CONFIRMATION));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPaymentNotificationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mConfirmPaymentReceiver);

        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}
